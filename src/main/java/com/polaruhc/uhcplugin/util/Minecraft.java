package com.polaruhc.uhcplugin.util;

import org.bukkit.Location;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class Minecraft {
    public static void spawnOrb(Location location, int exp){
        ExperienceOrb experienceOrb = location.getWorld().spawn(location, ExperienceOrb.class);
        experienceOrb.setExperience(exp);
    }

    public static void useItemDurability(Player player, boolean mainHand){
        PlayerInventory playerInventory = player.getInventory();
        ItemStack stack = mainHand ? playerInventory.getItemInMainHand() : playerInventory.getItemInOffHand();
        stack.setDurability((short) (stack.getDurability() + 1));
        if(stack.getDurability() == stack.getType().getMaxDurability()){
            stack.setAmount(stack.getAmount() - 1);
        }
        if(mainHand){
            playerInventory.setItemInMainHand(stack);
        }
        else{
            playerInventory.setItemInOffHand(stack);
        }
        player.updateInventory();
    }

    public static void spawnItem(Location location, ItemStack contents){
        Item item = location.getWorld().spawn(location, Item.class);
        item.setItemStack(contents);
    }
}
