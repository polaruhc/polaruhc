package com.polaruhc.uhcplugin.util;

import java.lang.reflect.Field;

public class Reflection {
    @SuppressWarnings("unchecked")
    public static <C,T> T access(Class<C> clazz, C object, String name) throws IllegalAccessException, NoSuchFieldException{
        Field field = clazz.getDeclaredField(name);
        field.setAccessible(true);
        return (T) field.get(object);
    }
}
