package com.polaruhc.uhcplugin.scatter;

import com.polaruhc.uhcplugin.UHC;
import com.polaruhc.uhcplugin.events.PostScatterEvent;
import com.polaruhc.uhcplugin.events.PreScatterEvent;
import com.polaruhc.uhcplugin.game.GameState;
import com.polaruhc.uhcplugin.user.User;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.boss.BarColor;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
A class for scattering players
**/
public class Scatterer implements Listener{
    private final UHC uhc;

    public Scatterer(UHC uhc) {
        this.uhc = uhc;
    }

    private List<Location> places = null;

    public void findPlaces(World world, int size, int space, Runnable finishcallback) {
        uhc.setGameState(GameState.SCATTER);
        uhc.broadcast(BarColor.GREEN, ChatColor.GRAY + "Finding scatter locations");
        uhc.broadcast(Sound.ENTITY_PLAYER_LEVELUP);
        places = new ArrayList<>();
        new BukkitRunnable() {
            int length = (size * 2) / space;
            boolean isNeg = true;
            int current = 0;
            boolean isZLeg = false;
            int x = size;
            int z = size;
            int done = 0;

            public void run() {
                if(uhc.getAnnounceBarCallBack() != null){
                    uhc.getAnnounceBarCallBack().cancel();
                    uhc.setAnnounceBarCallBack(null);
                }
                if (done >= uhc.getSlots() || length == 0) {
                    cancel();
                    Bukkit.getScheduler().runTaskLater(uhc, finishcallback, 20);
                    uhc.getAnnounceBar().setVisible(false);
                } else {
                    Chunk c = world.getChunkAt(x >>> 4, z >>> 4);
                    Location l = findSpot(c);
                    if (l != null) {
                        places.add(l);
                        uhc.getAnnounceBar().setTitle(ChatColor.GRAY + "Found scatter location [" + ChatColor.GREEN + done++ + ChatColor.GRAY + "/" + ChatColor.WHITE + uhc.getSlots() + ChatColor.GRAY + "]");
                        uhc.broadcast(Sound.BLOCK_NOTE_BLOCK_BASS);
                        uhc.getAnnounceBar().setColor(BarColor.GREEN);
                    }
                    else{
                        uhc.getAnnounceBar().setColor(BarColor.RED);
                        uhc.getAnnounceBar().setTitle(ChatColor.RED + "Failed to find scatter location [" + ChatColor.DARK_RED + done + ChatColor.RED + "/" + ChatColor.WHITE + uhc.getSlots() + ChatColor.RED + "]");
                        uhc.broadcast(Sound.ENTITY_BLAZE_HURT);
                    }

                    // make sure of the direction we're moving (X or Z? negative or positive?)
                    if (current < length)
                        current++;
                    else {    // one leg/side of the spiral down...
                        current = 0;
                        isZLeg =! isZLeg;
                        if (isZLeg) {    // every second leg (between X and Z legs, negative or positive), length increases
                            isNeg =! isNeg;
                            length--;
                        }
                    }

                    // move one area further in the appropriate direction
                    if (isZLeg) {
                        z += (isNeg) ? -space : space;
                    } else {
                        x += (isNeg) ? -space : space;
                    }

                    uhc.getAnnounceBar().setProgress((float)done/(float)uhc.getSlots());
                    uhc.getAnnounceBar().setVisible(true);
                }
            }
        }.runTaskTimer(uhc, 20L, 5L);
    }

    public Location findSpot(Chunk c){
        World world = c.getWorld();
        int worldx = c.getX() << 4;
        int worldz = c.getZ() << 4;
        for(int x = 0; x < 16; x ++){
            for(int z = 0; z < 16; z++){
                Block b = world.getHighestBlockAt(worldx + x, worldz + z);
                Material material = b.getType();
                if(material.isSolid() && material.isBlock()){
                    return b.getLocation().add(0.5, 1, 0.5);
                }
                else if(material != Material.LEGACY_STATIONARY_LAVA && material != Material.LAVA &&
                        (material = (b = b.getRelative(BlockFace.DOWN)).getType()).isSolid() && material.isBlock()){
                    return b.getLocation().add(0.5, 1, 0.5);
                }
            }
        }
        return null;
    }

    public void scatterSolos(){
        uhc.broadcast(BarColor.GREEN, ChatColor.GRAY + "Starting solo scatter...");
        uhc.broadcast(Sound.BLOCK_NOTE_BLOCK_BASS);
        uhc.getServer().getPluginManager().callEvent(new PreScatterEvent());
        List<User> sponsors = new ArrayList<>();
        List<User> players = new ArrayList<>();
        for(User user: uhc.getUsers()){
            if(user.isOnline()){
                (user.getPlayer().hasPermission("uhc.betterscatter") ? sponsors : players).add(user);
            }
        }
        Collections.shuffle(places);
        System.out.println("Starting scatter");
        scatterSponsorsSolos(sponsors, new Runnable() {
            public void run() {
                System.out.println("Scattered sponsors");
                scatterPlayerSolos(players, new Runnable() {
                    public void run() {
                        System.out.println("Scattered players");
                        finishScatter();
                    }
                });
            }
        });
    }

    public void finishScatter(){
        uhc.broadcast(BarColor.GREEN, ChatColor.GRAY + "Scatter complete");
        uhc.broadcast(Sound.ENTITY_PLAYER_LEVELUP);
        uhc.getServer().getPluginManager().callEvent(new PostScatterEvent());
        uhc.setGameState(GameState.INGAME);
    }

    public void scatterSponsorsSolos(List<User> sponsorsolos, Runnable finish){
        if(!sponsorsolos.isEmpty()) {
            uhc.broadcast(Sound.BLOCK_NOTE_BLOCK_BASS);
            Bukkit.broadcastMessage(UHC.PREFIX + "Scattering vips & sponsors solos");
            Iterator<Location> locationIterator = places.iterator();
            Iterator<User> userIterator = sponsorsolos.iterator();
            if(uhc.getAnnounceBarCallBack() != null){
                uhc.getAnnounceBarCallBack().cancel();
                uhc.setAnnounceBarCallBack(null);
            }
            new BukkitRunnable() {
                int i = 0;
                public void run() {
                    if (userIterator.hasNext() && locationIterator.hasNext()) {
                        User user = userIterator.next();
                        Location location = locationIterator.next();
                        if(user.isOnline()) {
                            user.getPlayer().teleport(location);
                            user.getPlayer().sendMessage(UHC.PREFIX + "Thank you for sponsoring Polar, enjoy the game!");
                            uhc.getAnnounceBar().setTitle(ChatColor.GRAY + "Scattering " +
                                    user.getRole().getPrefix() + user.getName()
                                    + ChatColor.GRAY + " [" + ChatColor.GREEN + i + ChatColor.GRAY + "/" + ChatColor.WHITE + sponsorsolos.size() + ChatColor.GRAY + "]");
                            uhc.getAnnounceBar().setColor(BarColor.GREEN);
                        }
                        else{
                            uhc.getAnnounceBar().setColor(BarColor.RED);
                            uhc.getAnnounceBar().setTitle(ChatColor.RED + "Removing " + ChatColor.GRAY + user.getName() + ChatColor.RED + " from the whitelist");
                            Iterator<OfflinePlayer> playerIterator = Bukkit.getWhitelistedPlayers().iterator();
                            while(playerIterator.hasNext()){
                                if(playerIterator.next().getUniqueId() == user.getUuid()){
                                    playerIterator.remove();
                                    break;
                                }
                            }
                        }
                        uhc.getAnnounceBar().setProgress((float)i/(float)sponsorsolos.size());
                        uhc.getAnnounceBar().setVisible(true);
                        i++;
                    }
                    cancel();
                    uhc.getAnnounceBar().setVisible(false);
                    if(finish != null) {
                        Bukkit.getScheduler().runTaskLater(uhc, finish, 20L);
                    }
                }
            }.runTaskTimer(uhc, 0L, 40L);
        }
    }

    public void scatterPlayerSolos(List<User> playersolos, Runnable finish){
        if(!playersolos.isEmpty()){
            uhc.broadcast(Sound.BLOCK_NOTE_BLOCK_BASS);
            Bukkit.broadcastMessage(UHC.PREFIX + "Scattering other solo players");
            Iterator<Location> locationIterator = places.iterator();
            Iterator<User> userIterator = playersolos.iterator();
            if(uhc.getAnnounceBarCallBack() != null){
                uhc.getAnnounceBarCallBack().cancel();
                uhc.setAnnounceBarCallBack(null);
            }
            new BukkitRunnable() {
                int i = 0;
                public void run() {
                    if (userIterator.hasNext() && locationIterator.hasNext()) {
                        User user = userIterator.next();
                        Location location = locationIterator.next();
                        if(user.isOnline()) {
                            user.getPlayer().teleport(location);
                            user.getPlayer().sendMessage(UHC.PREFIX + "Enjoy the game!");
                            uhc.getAnnounceBar().setTitle(ChatColor.GRAY + "Scattering " +
                                    user.getRole().getPrefix() + user.getName()
                                    + ChatColor.GRAY + " [" + ChatColor.WHITE + i + ChatColor.GRAY + "/" + ChatColor.WHITE + playersolos.size() + ChatColor.GRAY + "]");
                            uhc.getAnnounceBar().setColor(BarColor.YELLOW);
                        }
                        else{
                            uhc.getAnnounceBar().setColor(BarColor.RED);
                            uhc.getAnnounceBar().setTitle(ChatColor.RED + "Removing " + ChatColor.GRAY + user.getName() + ChatColor.RED + " from the whitelist");
                            Iterator<OfflinePlayer> playerIterator = Bukkit.getWhitelistedPlayers().iterator();
                            while(playerIterator.hasNext()){
                                if(playerIterator.next().getUniqueId() == user.getUuid()){
                                    playerIterator.remove();
                                    break;
                                }
                            }
                        }
                        uhc.getAnnounceBar().setProgress((float)i/(float)playersolos.size());
                        uhc.getAnnounceBar().setVisible(true);
                        i++;
                    }
                    cancel();
                    uhc.getAnnounceBar().setVisible(false);
                    if(finish != null) {
                        Bukkit.getScheduler().runTaskLater(uhc, finish, 20L);
                    }
                }
            }.runTaskTimer(uhc, 0L, 20L);
        }
    }
}
