package com.polaruhc.uhcplugin;

import com.comphenix.protocol.ProtocolLibrary;
import com.polaruhc.uhcplugin.commands.*;
import com.polaruhc.uhcplugin.game.*;
import com.polaruhc.uhcplugin.listeners.HostListener;
import com.polaruhc.uhcplugin.scatter.Scatterer;
import com.polaruhc.uhcplugin.scenario.ScenarioManager;
import com.polaruhc.uhcplugin.user.User;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;

public class UHC extends JavaPlugin {
    public static final String PREFIX = ChatColor.AQUA + ChatColor.BOLD.toString() + "Polar " + ChatColor.GRAY;

    private File db_file;
    private YamlConfiguration db_config;
    private GameState gameState;
    private UUID host;
    private int slots;
    private ConcurrentMap<UUID, User> userMap = new ConcurrentHashMap<>();
    private BossBar announceBar;
    private BukkitRunnable announceBarCallBack = null;

    private ScenarioManager scenarioManager = new ScenarioManager(this);
    private Scatterer scatterer = new Scatterer(this);

    private MessageListener messageListener = new MessageListener(this);
    private LoginListener loginListener = new LoginListener(this);
    private BasicCommand[] commands = {
            new HostCommand(this),
            new RoleCommand(this),
            new FinishGameCommand(this),
            new ScatterCommand(this),
    };

    public Collection<User> getUsers(){
        return userMap.values();
    }

    private static UHC instance;

    public void onEnable(){
        UHC.instance = this;
        quickLog("Loading database configuration");

        if(!getDataFolder().exists()){
            getDataFolder().mkdir();
        }
        db_file = new File(getDataFolder(), "settings.yml");
        if(!db_file.exists()){
            try {
                db_file.createNewFile();
            } catch (Throwable e) {
                exceptionLog(e, "Creating new configuration");
            }
        }

        db_config = new YamlConfiguration();
        try {
            db_config.load(db_file);
        } catch (Throwable e) {
            exceptionLog(e, "Loading configuration");
        }

        gameState = GameState.fromId(db_config.getInt("state", GameState.RESET.getId()));
        quickLog("Recovering with the gamestate " + gameState.getName() + " [" + gameState.getId() + "]");

        if(gameState.isBefore(GameState.FINISHED) && gameState.isAfter(GameState.PREHOST)) {
            host = UUID.fromString(db_config.getString("host"));
            quickLog("Using host: " + host);
        } else {
            host = null;
        }

        new BukkitRunnable(){
            public void run() {
                if(gameState.is(GameState.RESET)){
                    Bukkit.getWhitelistedPlayers().clear();
                    Bukkit.setWhitelist(true);
                    gameState = GameState.PREHOST;
                }
            }
        }.runTask(this);

        slots = db_config.getInt("slots", 100);
        quickLog("Using slots: " + slots);

        quickLog("Loading announce bar");

        announceBar = Bukkit.createBossBar("", BarColor.BLUE, BarStyle.SOLID);
        announceBar.setVisible(false);

        quickLog("Loading listeners");

        getServer().getPluginManager().registerEvents(messageListener, this);
        getServer().getPluginManager().registerEvents(loginListener, this);
        getServer().getPluginManager().registerEvents(new HostListener(), this);

        quickLog("Loading users");

        ConfigurationSection users = db_config.getConfigurationSection("users");
        if(users != null) {
            for (String index : users.getKeys(false)) {
                UUID uuid = UUID.fromString(index);
                ConfigurationSection usersection = users.getConfigurationSection(index);
                User user = new User(uuid);
                user.setName(usersection.getString("name"));
                user.setRole(GameRole.getRole(usersection.getInt("role", 0)));
                userMap.put(uuid, user);
            }
        } else {
            users = db_config.createSection("users");
        }

        quickLog("Loaded " + users.getKeys(false).size() + " users");

        quickLog("Processing " + Bukkit.getOnlinePlayers().size() + " online players");

        for(Player p: Bukkit.getOnlinePlayers()){
            loginListener.procesJoin(p);
        }

        messageListener.registerProtocolListeners();

        quickLog("Registering commands");

        for(BasicCommand basicCommand: commands){
            basicCommand.setPermission("uhc.command." + basicCommand.getPermission());
            basicCommand.setPermissionMessage(PREFIX + ChatColor.RED + "You do not have permission for this command");
            try {
                basicCommand.register();
                quickLog("Registered /" + basicCommand.getName());
            } catch (Throwable e) {
                exceptionLog(e, "Registering command: " + basicCommand.getName());
            }
        }
    }

    public int getSlots(){
        return slots;
    }

    public void broadcast(BarColor barColor, String string, Object ... replace){
        announceBar.setTitle(String.format(string, replace));
        if(announceBarCallBack != null){
            announceBarCallBack.cancel();
        }
        announceBar.setProgress(0);
        announceBar.setVisible(true);
        announceBar.setColor(barColor);

        announceBarCallBack = new BukkitRunnable() {
            final double num = 0.025;
            double sofar = 0;
            public void run() {
                if((sofar += num) > 1){
                    cancel();
                    announceBar.setVisible(false);
                    announceBarCallBack = null;
                }
                else announceBar.setProgress(sofar);
            }
        };
        announceBarCallBack.runTaskTimerAsynchronously(this, 0, 1);
    }

    public void broadcast(Sound sound){
        for(Player player: Bukkit.getOnlinePlayers()){
            player.playSound(player.getLocation(), sound, 1f, 1f);
        }
    }

    public String getHostName(){
        return getHost() == null ? "None" : getHost().getName();
    }

    public void onDisable(){
        try {
            BasicCommand.unregisterAll();
        } catch (Throwable e) {
            exceptionLog(e, "Unregistering commands");
        }

        ProtocolLibrary.getProtocolManager().removePacketListeners(this);

        db_config.set("host", host == null ? null : host.toString());
        db_config.set("slots", slots);

        for(User user: userMap.values()){
            String uuidstring = user.getUuid().toString();
            ConfigurationSection configurationSection = db_config.createSection("users." + uuidstring);
            configurationSection.set("name", user.getName());
            configurationSection.set("role", user.getRole().getId());
        }

        try {
            db_config.save(db_file);
        } catch (Throwable e) {
            exceptionLog(e, "Could not save db config");
        }

        announceBar.setVisible(false);
    }

    public ScenarioManager getScenarioManager() {
        return scenarioManager;
    }

    public void quickLog(String s){
        getLogger().log(Level.INFO, s);
    }

    public void exceptionLog(Throwable ex, String cause){
        getLogger().log(Level.SEVERE, cause + " ", ex);
    }

    public static String getPREFIX() {
        return PREFIX;
    }

    public BossBar getAnnounceBar() {
        return announceBar;
    }

    public BukkitRunnable getAnnounceBarCallBack() {
        return announceBarCallBack;
    }

    public ConcurrentMap<UUID, User> getUserMap() {
        return userMap;
    }

    public User getHost() {
        return host == null ? null : userMap.get(host);
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setAnnounceBarCallBack(BukkitRunnable announceBarCallBack) {
        this.announceBarCallBack = announceBarCallBack;
    }

    public void setSlots(int slots) {
        this.slots = slots;
    }

    public void setHost(User host) {
        this.host = host.getUuid();
        for(User user: getUsers()) {
            if(user.isOnline()) {
                messageListener.updaterHeader(user);
            }
        }
    }

    public void setHostUUID(UUID host){
        this.host = host;
        for(User user: getUsers()){
            if(user.isOnline()) {
                messageListener.updaterHeader(user);
            }
        }
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public YamlConfiguration getDb_config() {
        return db_config;
    }

    public File getDb_file() {
        return db_file;
    }

    public static String repeat(int count, String with) {
        return new String(new char[count]).replace("\0", with);
    }

    public LoginListener getLoginListener() {
        return loginListener;
    }

    public MessageListener getMessageListener() {
        return messageListener;
    }

    public BasicCommand[] getCommands() {
        return commands;
    }

    public Scatterer getScatterer(){
        return scatterer;
    }

    public static UHC getInstance() {
        return UHC.instance;
    }
}
