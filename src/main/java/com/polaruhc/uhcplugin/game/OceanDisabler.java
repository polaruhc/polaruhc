package com.polaruhc.uhcplugin.game;

import com.comphenix.protocol.wrappers.ChunkCoordIntPair;
import com.polaruhc.uhcplugin.UHC;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.block.Biome;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class OceanDisabler implements Listener{
    private final UHC uhc;
    private final List<Biome> disabled = Arrays.asList(Biome.OCEAN, Biome.DEEP_OCEAN, Biome.FROZEN_OCEAN, Biome.BEACH, Biome.SNOWY_BEACH);
    public OceanDisabler(UHC uhc) {
        this.uhc = uhc;
    }

    @EventHandler
    public void onChunkLoad(ChunkLoadEvent e){
        new BukkitRunnable(){
            public void run() {
                disableBiomes(e.getChunk(), e.isNewChunk());
            }
        }.runTask(uhc);
    }

    public void disableBiomes(Chunk c, boolean isNew){
        int worldCordX = c.getX() << 4;
        int worldCordZ = c.getZ() << 4;
        int maxY = c.getWorld().getMaxHeight();

        for(int x = 0; x < 16; x++){
            for(int z = 0; z < 16; z++){
                int yStart = Math.max(-64, c.getWorld().getMinHeight());
                for (int y = yStart; y <= maxY; y++) {
                    Location biomeLocation = new Location(c.getWorld(), worldCordX + x, y, worldCordZ + z);
                    //Biome biome = c.getWorld().getBiome(worldCordX + x, worldCordZ + z);
                    Biome biome = c.getWorld().getBiome(biomeLocation);
                    if (disabled.contains(biome)) {
                        //c.getWorld().setBiome(worldCordX + x, worldCordZ + z, Biome.PLAINS);
                        c.getWorld().setBiome(biomeLocation, Biome.PLAINS);
                    }
                }
            }
        }
    }
}
