package com.polaruhc.uhcplugin.game;

import net.md_5.bungee.api.ChatColor;

/**
 * A game role is a role that changes per player in a game
 */
public enum GameRole {
    PLAYER(ChatColor.GRAY + "[" + ChatColor.DARK_GRAY + "Player" + ChatColor.GRAY + "] ", ""),
    SPECTATOR(ChatColor.GRAY + "[" + ChatColor.GREEN + "Spectator" + ChatColor.GRAY + "] ", "Someone who spectates the UHC game"),
    MOD(ChatColor.GRAY + "[" + ChatColor.AQUA + "Mod" + ChatColor.GRAY + "] ", "Someone who moderates and spectates the UHC game"),
    HOST(ChatColor.GRAY + "[" + ChatColor.RED + "Host" + ChatColor.GRAY + "] ", "Someone who hosts and spectates the UHC game");

    public static GameRole getRole(int i){
        return values()[i];
    }

    private final String prefix, description;

    GameRole(String prefix, String description) {
        this.prefix = prefix;
        this.description = description;
    }

    public String getPrefix(){
        return prefix;
    }

    public String getDescription() {
        return description;
    }

    public int getId(){
        return ordinal();
    }

    public boolean canControl(GameState other){
        return getId() > other.getId();
    }

    public String getPermissionNeeded(){
        return "uhc.role." + toString().toLowerCase();
    }
}
