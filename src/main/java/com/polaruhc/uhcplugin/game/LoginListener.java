package com.polaruhc.uhcplugin.game;

import com.polaruhc.uhcplugin.UHC;
import com.polaruhc.uhcplugin.user.User;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;

import java.util.Date;
import java.util.UUID;

public class LoginListener implements Listener{
    private final UHC uhc;
    private final String login_header =
            ChatColor.AQUA + ChatColor.BOLD.toString() + "PolarUHC\n"
            + ChatColor.GRAY + ">" + ChatColor.STRIKETHROUGH + UHC.repeat(100, " ") + ChatColor.GRAY + "<"
            + "\n\n" + ChatColor.RED;
    private final String login_footer =
            "\n\n" + ChatColor.GRAY + ">" + ChatColor.STRIKETHROUGH + UHC.repeat(100, " ") + ChatColor.GRAY + "<"
            + "\n" + ChatColor.DARK_AQUA + ChatColor.ITALIC.toString() + "@PolarUHC";

    public LoginListener(UHC uhc) {
        this.uhc = uhc;
    }

    public boolean isWhitelisted(UUID uuid){
        for(OfflinePlayer player: Bukkit.getWhitelistedPlayers()) {
            if (player.getUniqueId() == uuid)
                return true;
        }
        return false;
    }

    public boolean isWhitelisted(String name){
        for(OfflinePlayer player: Bukkit.getWhitelistedPlayers()){
            if(player.getName().equals(name)){
                return true;
            }
        }
        return false;
    }

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent e){
        Player p = e.getPlayer();
        PlayerLoginEvent.Result result = e.getResult();
        User user = procesJoin(p);

        if(result == PlayerLoginEvent.Result.ALLOWED || result == PlayerLoginEvent.Result.KICK_FULL){
            int online = 0;

            for(User onlineuser: uhc.getUsers()){
                if(onlineuser.isOnline() && onlineuser.needsScatter()){
                    online++;
                }
            }

            if (online >= uhc.getSlots()){
                e.setResult(PlayerLoginEvent.Result.KICK_FULL);
            } else {
                e.allow();
            }
        }

        switch (result){
            case KICK_WHITELIST:
                if(user != null && user.bypassWhitelist()){
                    e.allow();
                } else {
                    e.setKickMessage(login_header + "The whitelist is on" + login_footer);
                }
                break;
            case KICK_FULL:
                if(user != null && user.bypassFull()) {
                    e.allow();
                } else {
                    e.setKickMessage(login_header + "The server is full" + login_footer);
                }
                break;
            case KICK_BANNED:
                BanList banList = uhc.getServer().getBanList(BanList.Type.NAME);
                String ban_message = ChatColor.DARK_RED + "You are banned \n" + ChatColor.RED;
                if(banList.isBanned(p.getName())){
                    BanEntry banEntry = banList.getBanEntry(p.getName());
                    ban_message += "\nReason: " + ChatColor.GRAY + (banEntry.getReason() == null ? "The ice bear has spoken" : banEntry.getReason()) + ChatColor.RED;
                    if(banEntry.getSource() != null) ban_message += "\nBanned by: " + ChatColor.GRAY + banEntry.getSource() + ChatColor.RED;
                    if(banEntry.getCreated() != null) ban_message += "\nCreated: " + ChatColor.GRAY + banEntry.getCreated().toGMTString().replace("GMT", "UTC") + ChatColor.RED;
                    ban_message += "\nExpires: " + ChatColor.GRAY + (banEntry.getExpiration() == null ? "Never" : banEntry.getExpiration().toGMTString().replace("GMT", "UTC")) + ChatColor.RED;
                }
                else if((banList = uhc.getServer().getBanList(BanList.Type.IP)).isBanned(p.getName())){
                    BanEntry banEntry = banList.getBanEntry(p.getName());
                    ban_message += "\nReason: " + ChatColor.GRAY + (banEntry.getReason() == null ? "The ice bear has spoken" : banEntry.getReason()) + ChatColor.RED;
                    if(banEntry.getSource() != null) ban_message += "\nBanned by: " + ChatColor.GRAY + banEntry.getSource() + ChatColor.RED;
                    if(banEntry.getCreated() != null) ban_message += "\nCreated: " + ChatColor.GRAY + banEntry.getCreated().toGMTString().replace("GMT", "UTC") + ChatColor.RED;
                    ban_message += "\nExpires: " + ChatColor.GRAY + (banEntry.getExpiration() == null ? "Never" : banEntry.getExpiration().toGMTString().replace("GMT", "UTC")) + ChatColor.RED;
                }
                e.setKickMessage(login_header + ban_message + login_footer);
                break;
            case KICK_OTHER:
                e.setKickMessage(login_header + e.getKickMessage() + login_footer);
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null); // no join message

        // get player and add them ot the announcement bar
        Player p = e.getPlayer();
        uhc.getAnnounceBar().addPlayer(p);
        User user = uhc.getUserMap().get(p.getUniqueId());
        if (user != null) {
            // add player to bossbar
            user.setPlayer(p);
            user.getBossBar().addPlayer(p);
        }
    }

    public void ban(OfflinePlayer player, String source, String reason, Date unban, BanList banList){
        BanEntry banEntry;
        if(banList.isBanned(player.getName())){
            banEntry = banList.getBanEntry(player.getName());
            banEntry.setSource(source);
            banEntry.setReason(reason);
            banEntry.setExpiration(unban);
            banEntry.setCreated(new Date());
        }
        else{
            banEntry = banList.addBan(player.getName(), reason, unban, source);
        }
        banEntry.save();
        if(player.isOnline()){
            String ban_message = ChatColor.DARK_RED + "You are banned \n" + ChatColor.RED;
            ban_message += "\nReason: " + ChatColor.GRAY + (banEntry.getReason() == null ? "The ice bear has spoken" : banEntry.getReason()) + ChatColor.RED;
            if(banEntry.getSource() != null) ban_message += "\nBanned by: " + ChatColor.GRAY + banEntry.getSource() + ChatColor.RED;
            if(banEntry.getCreated() != null) ban_message += "\nCreated: " + ChatColor.GRAY + banEntry.getCreated().toGMTString().replace("GMT", "UTC") + ChatColor.RED;
            ban_message += "\nExpires: " + ChatColor.GRAY + (banEntry.getExpiration() == null ? "Never" : banEntry.getExpiration().toGMTString().replace("GMT", "UTC")) + ChatColor.RED;
            player.getPlayer().kickPlayer(ban_message);
        }
    }

    @EventHandler
    public void onPlayerKick(PlayerKickEvent e){
        uhc.getAnnounceBar().removePlayer(e.getPlayer());
        e.setLeaveMessage(login_header + e.getReason() + login_footer);
    }

    public User procesJoin(Player p){
        User user = uhc.getUserMap().get(p.getUniqueId());
        if(user == null) {
            user = new User(p.getUniqueId());
            uhc.getUserMap().put(p.getUniqueId(), user);
        } else {
            if (user.getRole() == GameRole.HOST && user != uhc.getHost()){
                user.setRole(GameRole.MOD);
            }
        }
        user.setName(p.getName());
        return user;
    }

    public String getLogin_header() {
        return login_header;
    }

    public String getLogin_footer() {
        return login_footer;
    }
}
