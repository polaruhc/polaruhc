package com.polaruhc.uhcplugin.game;

import com.polaruhc.uhcplugin.packets.WrapperPlayServerPlayerInfo;
import com.polaruhc.uhcplugin.packets.WrapperPlayServerPlayerListHeaderFooter;
import com.polaruhc.uhcplugin.packets.WrapperStatusServerServerInfo;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListeningWhitelist;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.*;
import com.polaruhc.uhcplugin.UHC;
import com.polaruhc.uhcplugin.scenario.Scenario;
import com.polaruhc.uhcplugin.user.User;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * The Bubble Network 2016
 * PolarUHC
 * 7/27/2016 {2:48 PM}
 * Created July 2016
 */
public class MessageListener implements Listener{
    private static UUID[] uuids = new UUID[20];

    {
        for(int i = 0; i < uuids.length; i++){
            uuids[i] = UUID.randomUUID();
        }
    }

    private final UHC uhc;
    private boolean muted = false;

    public MessageListener(UHC uhc) {
        this.uhc = uhc;
    }

    public void registerProtocolListeners(){
        ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(uhc, PacketType.Status.Server.SERVER_INFO, PacketType.Play.Server.PLAYER_INFO) {
            public void onPacketReceiving(PacketEvent event) {
            }

            public void onPacketSending(PacketEvent event) {
                if(event.getPacketType() == PacketType.Status.Server.SERVER_INFO){
                    WrapperStatusServerServerInfo wrapperStatusServerServerInfo = new WrapperStatusServerServerInfo(event.getPacket());
                    WrappedServerPing wrappedServerPing = wrapperStatusServerServerInfo.getJsonResponse();
                    int online;
                    if(uhc.getGameState().isAfter(GameState.POSTHOST) && uhc.getGameState().isBefore(GameState.RESET)) {
                        online = 0;
                        for (User user : uhc.getUsers()) {
                            if (user.isOnline() && user.needsScatter()) {
                                online++;
                            }
                        }
                        wrappedServerPing.setPlayersOnline(online);
                    }
                    else{
                        online = wrappedServerPing.getPlayersOnline();
                    }
                    if(uhc.getGameState().isBefore(GameState.LOBBY) || uhc.getGameState() == GameState.RESET){
                        wrappedServerPing.setVersionName(ChatColor.YELLOW + (uhc.getHost() == null ? "No host yet " : "Not ready yet "));
                        wrappedServerPing.setVersionProtocol(0);
                    }
                    else if(uhc.getGameState().isAfter(GameState.INGAME)){
                        wrappedServerPing.setVersionName(ChatColor.YELLOW + "Game has finished ");
                        wrappedServerPing.setVersionProtocol(0);
                    }
                    else if(uhc.getGameState().isAfter(GameState.LOBBY)){
                        wrappedServerPing.setVersionName(ChatColor.YELLOW + "Game has started " + ChatColor.GRAY + online + " Playing ");
                        wrappedServerPing.setVersionProtocol(0);
                    }
                    else if(Bukkit.hasWhitelist()){
                        wrappedServerPing.setVersionName(ChatColor.GRAY + "Server Whitelisted");
                        wrappedServerPing.setVersionProtocol(0);
                    }
                    else{
                        wrappedServerPing.setVersionName("Polar");
                    }
                    String scenarios = uhc.getScenarioManager().getScenarioList().isEmpty() ? "Vanilla " : "";
                    for(Scenario scenario: uhc.getScenarioManager().getScenarioList()){
                        scenarios += " " + scenario.getName() + ",";
                    }
                    String[] lines = {UHC.PREFIX,
                            ChatColor.GRAY + ChatColor.STRIKETHROUGH.toString() + ">" + UHC.repeat(50, " ") + "<",
                            ChatColor.RESET.toString(),
                            ChatColor.BLUE + "Host: " + ChatColor.WHITE + uhc.getHostName(),
                            ChatColor.BLUE + "Scenarios: " + ChatColor.WHITE + scenarios.substring(0, scenarios.length() - 1),
                            ChatColor.RESET.toString() + ChatColor.RESET.toString(),
                            ChatColor.RESET.toString() + ChatColor.GRAY + ChatColor.STRIKETHROUGH.toString() + ">" + UHC.repeat(50, " ") + "<",
                    };

                    List<WrappedGameProfile> wrappedGameProfileList = new ArrayList<>();
                    int i = 0;
                    for(String s: lines){
                        wrappedGameProfileList.add(new WrappedGameProfile(uuids[i], s));
                        i++;
                    }
                    wrappedServerPing.setPlayersMaximum(uhc.getSlots());
                    wrappedServerPing.setPlayers(wrappedGameProfileList);
                    wrapperStatusServerServerInfo.setJsonResponse(wrappedServerPing);
                    event.setPacket(wrapperStatusServerServerInfo.getHandle());
                }

                if(event.getPacketType() == PacketType.Play.Server.PLAYER_INFO){
                    WrapperPlayServerPlayerInfo wrapperPlayServerPlayerInfo = new WrapperPlayServerPlayerInfo(event.getPacket());
                    List<PlayerInfoData> playerInfoDatas = new ArrayList<>();
                    for(PlayerInfoData playerInfoData: wrapperPlayServerPlayerInfo.getData()){
                        int latency = playerInfoData.getLatency();
                        WrappedGameProfile gameProfile = playerInfoData.getProfile();
                        EnumWrappers.NativeGameMode nativeGameMode = playerInfoData.getGameMode();
                        WrappedChatComponent wrappedChatComponent = playerInfoData.getDisplayName();
                        if(uhc.getGameState().isAfter(GameState.POSTHOST) && uhc.getGameState().isBefore(GameState.RESET)
                                && wrapperPlayServerPlayerInfo.getAction() == EnumWrappers.PlayerInfoAction.ADD_PLAYER) {
                            User user = uhc.getUserMap().get(playerInfoData.getProfile().getUUID());
                            if(!user.needsScatter()){
                                continue;
                            }
                        }
                        latency = 0;
                        playerInfoDatas.add(new PlayerInfoData(gameProfile, latency, nativeGameMode, wrappedChatComponent));
                    }
                    wrapperPlayServerPlayerInfo.setData(playerInfoDatas);
                    event.setPacket(wrapperPlayServerPlayerInfo.getHandle());
                }
            }

        });
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerJoin(PlayerJoinEvent e){
        e.setJoinMessage(null);
        Player p = e.getPlayer();
        int i;
        if(uhc.getGameState().isAfter(GameState.POSTHOST) && uhc.getGameState().isBefore(GameState.FINISHED)) {
            i = 0;
            for (User user : uhc.getUsers()) {
                if (user.isOnline() && user.needsScatter()) {
                    i++;
                }
            }
        } else {
            i = Bukkit.getOnlinePlayers().size();
        }

        for(User user: uhc.getUsers()){
            if(user.isOnline()){
                updaterHeader(user, i);
            }
        }

        User user = uhc.getUserMap().get(p.getUniqueId());
        uhc.broadcast(BarColor.BLUE, ChatColor.DARK_GREEN + ChatColor.BOLD.toString() + "[+] " + ChatColor.RESET + user.getMessagePrefix());
        uhc.broadcast(Sound.BLOCK_NOTE_BLOCK_BASS);
    }

    public void updaterHeader(User user){
        String scenarios = uhc.getScenarioManager().getScenarioList().isEmpty() ? "Vanilla " : "";
        for(Scenario scenario: uhc.getScenarioManager().getScenarioList()){
            scenarios += " " + scenario.getName() + ",";
        }
        String header = UHC.PREFIX + "\n"
                + ChatColor.GRAY + ">" + ChatColor.STRIKETHROUGH.toString() + UHC.repeat(50, " ") + ChatColor.GRAY + "<";
        String footer = "\n" + ChatColor.BLUE + "Host: " + ChatColor.WHITE + uhc.getHostName() +
                "\n" + ChatColor.BLUE + "Scenarios: " + ChatColor.WHITE + scenarios.substring(0, scenarios.length() - 1) +
                "\n" + ChatColor.BLUE + "Players: " + ChatColor.WHITE;
        if(uhc.getGameState().isAfter(GameState.POSTHOST) && uhc.getGameState().isBefore(GameState.FINISHED)){
            int i = 0;
            for(User otheruser: uhc.getUsers()){
                if(otheruser.isOnline() && otheruser.needsScatter()){
                    i++;
                }
            }
            footer += i + "/" + uhc.getSlots();
        }
        else{
            footer += Bukkit.getOnlinePlayers().size() + "/" + uhc.getSlots();
        }
        footer += "\n" + ChatColor.GRAY + ">" + ChatColor.STRIKETHROUGH.toString() + UHC.repeat(50, " ") + ChatColor.GRAY + "<";
        user.setHeaderFooter(uhc, header, footer);
    }

    public void updaterHeader(User user, int online){
        String scenarios = uhc.getScenarioManager().getScenarioList().isEmpty() ? "Vanilla " : "";
        for(Scenario scenario: uhc.getScenarioManager().getScenarioList()){
            scenarios += " " + scenario.getName() + ",";
        }
        String header = UHC.PREFIX + "\n"
                + ChatColor.GRAY + ">" + ChatColor.STRIKETHROUGH.toString() + UHC.repeat(50, " ") + ChatColor.GRAY + "<";
        String footer = "\n" + ChatColor.BLUE + "Host: " + ChatColor.WHITE + uhc.getHostName() +
                "\n" + ChatColor.BLUE + "Scenarios: " + ChatColor.WHITE + scenarios.substring(0, scenarios.length() - 1) +
                "\n" + ChatColor.BLUE + "Players: " + ChatColor.WHITE + online + "/" + uhc.getSlots() +
                "\n" + ChatColor.GRAY + ">" + ChatColor.STRIKETHROUGH.toString() + UHC.repeat(50, " ") + ChatColor.GRAY + "<";
        user.setHeaderFooter(uhc, header, footer);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e){
        Player p = e.getPlayer();
        User user = uhc.getUserMap().get(p.getUniqueId());
        uhc.getAnnounceBar().removePlayer(p);
        user.cleanUp();
        uhc.broadcast(BarColor.BLUE, ChatColor.DARK_RED + ChatColor.BOLD.toString() + "[-] " + ChatColor.RESET + user.getMessagePrefix());
        uhc.broadcast(Sound.BLOCK_NOTE_BLOCK_BASS);
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e){
        e.setCancelled(true);
        Player player = e.getPlayer();
        User user = uhc.getUserMap().get(player.getUniqueId());

        if(e.getMessage().startsWith("@")) {
            String target = e.getMessage().substring(1, e.getMessage().length()).split(" ")[0];
            Player targetplayer = Bukkit.getPlayer(target);

            if(targetplayer == null){
                user.sendBossBar(uhc, BarColor.RED,  ChatColor.RED + "Player not found");
                user.play(Sound.ENTITY_BLAZE_HURT);
            } else {
                String message = e.getMessage().substring(target.length() + 2, e.getMessage().length());
                if(message.isEmpty()){
                    user.sendBossBar(uhc, BarColor.RED, ChatColor.RED + "Include a message!");
                    user.play(Sound.ENTITY_BLAZE_HURT);
                }
                else {
                    player.sendMessage(ChatColor.GRAY + "[You -> " + targetplayer.getName() + "] " + ChatColor.WHITE + message);
                    targetplayer.sendMessage(ChatColor.GRAY + "[" + player.getName() + " -> You] " + ChatColor.WHITE + message);
                    targetplayer.playSound(targetplayer.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1f, 1f);
                }
            }

        } else if(muted && !user.isStaff()){
            user.sendBossBar(uhc, BarColor.RED, ChatColor.RED + "Chat is currently muted!");
            user.play(Sound.ENTITY_BLAZE_HURT);
        } else {
            Bukkit.getConsoleSender().sendMessage(user.getMessagePrefix() + " " + e.getMessage());
            BaseComponent[] componentmessage = TextComponent.fromLegacyText(user.getMessagePrefix() + " " + e.getMessage());
            ClickEvent clickEvent = new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "@" + player.getName() + " ");
            HoverEvent hoverEvent = null;

            if(user.getRole() != GameRole.PLAYER){
                String text = user.getRole().getPrefix();
                text += "\n\n";
                text += ChatColor.GRAY;
                text += user.getRole().getDescription();
                hoverEvent = new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText(text));
            }

            for(BaseComponent baseComponent: componentmessage){
                baseComponent.setClickEvent(clickEvent);
                if(hoverEvent != null){
                    baseComponent.setHoverEvent(hoverEvent);
                }
            }

            for (Player p: e.getRecipients()) {
                p.spigot().sendMessage(ChatMessageType.SYSTEM, componentmessage);
            }

        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e){
        uhc.broadcast(BarColor.PURPLE, e.getDeathMessage());
        e.setDeathMessage(null);
    }

    public boolean isMuted() {
        return muted;
    }

    public void setMuted(boolean muted) {
        this.muted = muted;
    }
}
