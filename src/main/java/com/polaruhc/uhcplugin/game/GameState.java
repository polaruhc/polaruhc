package com.polaruhc.uhcplugin.game;

public enum GameState {
    PREHOST, POSTHOST, GENNING, LOBBY, SCATTER, INGAME, FINISHED, RESET;

    private String name;

    GameState(){
        name = String.valueOf(toString().charAt(0)).toUpperCase() + toString().toLowerCase().substring(1);
    }

    public String getName(){
        return name;
    }

    public static GameState fromId(int id){
        return values()[id];
    }

    public static GameState fromName(String name){
        return valueOf(name.toUpperCase());
    }

    public int getId(){
        return ordinal();
    }

    public boolean is(GameState gameState){
        return gameState.getId() == getId();
    }

    public boolean isBefore(GameState gameState){
        return getId() < gameState.getId();
    }

    public boolean isAfter(GameState gameState){
        return getId() > gameState.getId();
    }
}
