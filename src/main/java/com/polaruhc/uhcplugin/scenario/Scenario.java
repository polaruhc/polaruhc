package com.polaruhc.uhcplugin.scenario;

import com.polaruhc.uhcplugin.UHC;
import com.polaruhc.uhcplugin.commands.BasicCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.util.Collections;

/**
 * Created by Administrator on 27/07/2016.
 */
public abstract class Scenario implements Listener{
    private final UHC uhc;
    private final ScenarioManager scenarioManager;
    private final String name, description;

    public Scenario(UHC uhc, ScenarioManager scenarioManager, String name, String description) {
        this.uhc = uhc;
        this.scenarioManager = scenarioManager;
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public boolean isEnabled(){
        return uhc.getScenarioManager().getScenarioList().contains(this);
    }

    public void init(){
        Bukkit.getPluginManager().registerEvents(this, uhc);
    }

    public void kill(){
        HandlerList.unregisterAll(this);
    }

    public String getPrefix(){
        return ChatColor.GREEN + "[" + name + "] " + ChatColor.RESET;
    }
}
