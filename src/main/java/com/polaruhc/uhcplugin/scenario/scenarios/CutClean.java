package com.polaruhc.uhcplugin.scenario.scenarios;

import com.polaruhc.uhcplugin.UHC;
import com.polaruhc.uhcplugin.scenario.Scenario;
import com.polaruhc.uhcplugin.scenario.ScenarioManager;
import com.polaruhc.uhcplugin.util.Minecraft;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class CutClean extends Scenario
{
    private final UHC uhc;
    private final ScenarioManager scenarioManager;

    public CutClean(UHC uhc, ScenarioManager scenarioManager) {
        super(uhc, scenarioManager, "CutClean", "All ores and raw food are auto-smelted");
        this.uhc = uhc;
        this.scenarioManager = scenarioManager;
    }

    @Override
    public void kill() {
        super.kill();
    }

    @Override
    public void init() {
        super.init();
    }

    @EventHandler(ignoreCancelled = true)
    public void onAnimalDeath(EntityDeathEvent e){
        Entity entity = e.getEntity();
        if(entity instanceof Animals){
            Animals animal = (Animals)entity;
            List<ItemStack> drops = e.getDrops();
            switch (animal.getType()) {
                case CHICKEN:
                    drops.clear();
                    int featheramount = ThreadLocalRandom.current().nextInt(3);
                    if (featheramount > 0) {
                        drops.add(new ItemStack(Material.FEATHER, featheramount));
                    }
                    if(animal.isAdult()) drops.add(new ItemStack(Material.COOKED_CHICKEN, 1));
                    break;
                case COW:
                case MUSHROOM_COW:
                    drops.clear();
                    int leatheramount = ThreadLocalRandom.current().nextInt(3);
                    if (leatheramount > 0) {
                        drops.add(new ItemStack(Material.LEATHER, leatheramount));
                    }
                    if(animal.isAdult()) {
                        int meatamount = ThreadLocalRandom.current().nextInt(3) + 1;
                        drops.add(new ItemStack(Material.COOKED_BEEF, meatamount));
                    }
                    break;
                case PIG:
                    drops.clear();
                    Pig pig = (Pig)animal;
                    if(pig.hasSaddle()){
                        drops.add(new ItemStack(Material.SADDLE, 1));
                    }
                    if(animal.isAdult()) {
                        int meatamount = ThreadLocalRandom.current().nextInt(3) + 1;
                        drops.add(new ItemStack(Material.COOKED_PORKCHOP, meatamount));
                    }
                    break;
                case RABBIT:
                   drops.clear();
                    if(animal.isAdult()){
                        int hideamount = ThreadLocalRandom.current().nextInt(2);
                        if(hideamount > 0){
                            drops.add(new ItemStack(Material.RABBIT_HIDE, hideamount));
                        }
                        int meatamount = ThreadLocalRandom.current().nextInt(2);
                        if(meatamount > 0){
                            drops.add(new ItemStack(Material.COOKED_RABBIT, meatamount));
                        }
                        if(animal.getKiller() != null && ThreadLocalRandom.current().nextInt(10) == 0){
                            drops.add(new ItemStack(Material.RABBIT_FOOT, 1));
                        }
                    }
                    break;
                case SHEEP:
                    drops.clear();
                    Sheep sheep = (Sheep)animal;
                    if(!sheep.isSheared()){
                        drops.add(new ItemStack(Material.BLACK_WOOL, 1, sheep.getColor().getDyeData()));
                    }
                    if(animal.isAdult()){
                        drops.add(new ItemStack(Material.COOKED_MUTTON, 1 + ThreadLocalRandom.current().nextInt(3)));
                    }
                    break;
                case POLAR_BEAR:
                    drops.clear();
                    int fishamount = ThreadLocalRandom.current().nextInt(3);
                    if(fishamount > 0){
                        drops.add(new ItemStack(Material.COOKED_COD, fishamount));
                    }
                    int salmonamount = ThreadLocalRandom.current().nextInt(3);
                    if(salmonamount > 0){
                        drops.add(new ItemStack(Material.COOKED_SALMON, salmonamount));
                    }
                    break;
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent e){
        Block block = e.getBlock();
        if(!scenarioManager.isEnabled(scenarioManager.TRIPLE_ORES) && e.getPlayer().getGameMode() == GameMode.SURVIVAL){
            ItemStack drop = null;
            switch (block.getType()){
                case IRON_ORE:
                    drop = new ItemStack(Material.IRON_INGOT);
                    break;
                case GOLD_ORE:
                    drop = new ItemStack(Material.GOLD_ORE);
                    break;
            }
            if(drop != null){
                block.setType(Material.AIR);
                Minecraft.useItemDurability(e.getPlayer(), false);
                Minecraft.spawnItem(block.getLocation().add(0.5, 0.5, 0.5), drop);
                Minecraft.spawnOrb(block.getLocation().add(0.5, 0.5, 0.5), e.getExpToDrop());
                e.setCancelled(true);
            }
        }
    }
}
