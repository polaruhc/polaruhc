package com.polaruhc.uhcplugin.scenario;

import com.polaruhc.uhcplugin.UHC;
import com.polaruhc.uhcplugin.scenario.scenarios.CutClean;
import com.polaruhc.uhcplugin.scenario.scenarios.TripleOres;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 27/07/2016.
 */
public class ScenarioManager {
    private static final Scenario[] scenarios = {};

    public static Scenario[] getScenarios() {
        return scenarios;
    }

    private final UHC uhc;
    private List<Scenario> scenarioList = new ArrayList<>();

    public final CutClean CUT_CLEAN;
    public final TripleOres TRIPLE_ORES;

    public ScenarioManager(UHC uhc) {
        this.uhc = uhc;
        CUT_CLEAN = new CutClean(uhc, this);
        TRIPLE_ORES = new TripleOres(uhc, this);
    }

    public void loadScenarios(List<String> scenarios){
        scenarioList = new ArrayList<>();
        for(Scenario scenario: ScenarioManager.scenarios){
            if(scenarios.contains(scenario.getName())){
                scenarioList.add(scenario);
            }
        }
    }

    public boolean isEnabled(Scenario scenario){
        return scenarioList.contains(scenario);
    }

    public void init(){
        for(Scenario scenario: scenarioList){
            scenario.init();
        }
    }

    public void kill(){
        for(Scenario scenario: scenarioList){
            scenario.kill();
        }
    }

    public List<Scenario> getScenarioList() {
        return scenarioList;
    }
}
