package com.polaruhc.uhcplugin.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ScatterEvent extends Event{
    private static HandlerList handlerList = new HandlerList();

    public ScatterEvent() {
        super(false);
    }

    public HandlerList getHandlers(){
        return handlerList;
    }

    public static HandlerList getHandlerList(){
        return handlerList;
    }
}
