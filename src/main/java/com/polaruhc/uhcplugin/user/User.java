package com.polaruhc.uhcplugin.user;

import com.polaruhc.uhcplugin.packets.WrapperPlayServerPlayerListHeaderFooter;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.polaruhc.uhcplugin.UHC;
import com.polaruhc.uhcplugin.game.GameRole;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

/**
 * The Bubble Network 2016
 * PolarUHC
 * 7/26/2016 {9:02 PM}
 * Created July 2016
 */
public class User {
    private final UUID uuid;
    private String name = null;
    private Player player = null;
    private GameRole role = GameRole.PLAYER;
    private BossBar bossBar = Bukkit.createBossBar("", BarColor.PURPLE, BarStyle.SOLID);
    private BukkitRunnable bossBarCallBack = null;

    public User(UUID uuid) {
        this.uuid = uuid;
        bossBar.setVisible(false);
    }

    public String getMessagePrefix(){
        return role.getPrefix() + getName();
    }

    public void setRole(GameRole role) {
        this.role = role;
    }

    public Player getPlayer() {
        return player;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPlayer(Player player) {
        this.player = player;
        this.name = player.getName();
    }

    public void cleanUp(){
        this.bossBar.removePlayer(player);
    }


    public GameRole getRole(){
        return role;
    }

    public boolean isOnline(){
        return player != null && player.isOnline();
    }

    public boolean needsScatter(){
        return role == GameRole.PLAYER;
    }

    public boolean isStaff(){
        return role == GameRole.HOST || role == GameRole.MOD;
    }

    public boolean bypassWhitelist(){
        return role != GameRole.PLAYER;
    }

    public boolean bypassFull(){
        return role != GameRole.PLAYER || (player.hasPermission("polaruhc.joinfull"));
    }

    public BossBar getBossBar() {
        return this.bossBar;
    }

    public void sendBossBar(UHC uhc, BarColor barColor, String message){
        if(bossBarCallBack != null){
            bossBarCallBack.cancel();
        }
        bossBar.setTitle(message);
        bossBar.setProgress(0.0);
        bossBar.setVisible(true);
        bossBar.setColor(barColor);
        bossBarCallBack = new BukkitRunnable() {
            final double num = 0.025;
            double sofar = 0;
            public void run() {
                if((sofar += num) > 1){
                    cancel();
                    bossBar.setVisible(false);
                    bossBarCallBack = null;
                }
                else bossBar.setProgress(sofar);
            }
        };
        bossBarCallBack.runTaskTimerAsynchronously(uhc, 0, 1);
    }

    public void play(Sound sound){
        player.playSound(player.getLocation(), sound, 1f, 1f );
    }

    public boolean canChangeRole(GameRole gameRole){
        return player.hasPermission(gameRole.getPermissionNeeded());
    }
    public void setHeaderFooter(UHC uhc, String headerstring, String footerstring){
        WrapperPlayServerPlayerListHeaderFooter playServerPlayerListHeaderFooter = new WrapperPlayServerPlayerListHeaderFooter();
        playServerPlayerListHeaderFooter.setHeader(WrappedChatComponent.fromText(headerstring));
        playServerPlayerListHeaderFooter.setFooter(WrappedChatComponent.fromText(footerstring));
        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(player, playServerPlayerListHeaderFooter.getHandle());
        } catch (Throwable e1) {
            uhc.exceptionLog(e1, "Could not send tab packet");
        }
    }
}
