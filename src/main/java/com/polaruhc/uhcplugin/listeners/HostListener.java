package com.polaruhc.uhcplugin.listeners;

import com.polaruhc.uhcplugin.UHC;
import com.polaruhc.uhcplugin.game.GameRole;
import com.polaruhc.uhcplugin.user.User;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class HostListener implements Listener {
    @EventHandler(priority= EventPriority.LOW)
    public void onHostLogin(PlayerLoginEvent event) {
        // By default first person who connects is made host
        if (UHC.getInstance().getHost() == null && (event.getPlayer().hasPermission("polaruhc.unctrial") || event.getPlayer().isOp())) {
            UHC.getInstance().setHostUUID(event.getPlayer().getUniqueId());
        }

        if (UHC.getInstance().getHost() != null && UHC.getInstance().getHost().getUuid().equals(event.getPlayer().getUniqueId())) {
            event.setResult(PlayerLoginEvent.Result.ALLOWED);
        }
    }
}
