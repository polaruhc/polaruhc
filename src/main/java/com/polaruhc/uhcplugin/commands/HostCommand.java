package com.polaruhc.uhcplugin.commands;

import com.polaruhc.uhcplugin.UHC;
import com.polaruhc.uhcplugin.game.GameRole;
import com.polaruhc.uhcplugin.user.User;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collections;

public class HostCommand extends BasicCommand{
    private final UHC uhc;

    public HostCommand(UHC uhc ){
        super(uhc, "sethost", "Set the host of a UHC game", "/<command> <host/none>", Collections.emptyList());
        this.uhc = uhc;
    }

    public boolean execute(CommandSender commandSender, String label, String[] args) {
        if(args.length < 1){
            if(commandSender instanceof Player){
                Player player = (Player) commandSender;
                User user = uhc.getUserMap().get(player.getUniqueId());
                user.sendBossBar(uhc, BarColor.RED, ChatColor.RED + "Invalid usage: " + ChatColor.YELLOW +  getUsage().replace("<command>", label));
                user.play(Sound.ENTITY_BLAZE_HURT);
            }
            else commandSender.sendMessage(ChatColor.RED + "Invalid usage: " + ChatColor.YELLOW + "/" + getUsage().replace("<command>", label));
        }
        else{
            if(args[0].equalsIgnoreCase("none")){
                if(uhc.getHost() != null){
                    uhc.getHost().setRole(GameRole.MOD);
                    if(uhc.getHost().isOnline()){
                        uhc.getHost().getPlayer().sendMessage(UHC.PREFIX + ChatColor.GRAY + "You are no longer " + GameRole.HOST.getPrefix());
                    }
                }
                uhc.setHostUUID(null);
                uhc.broadcast(BarColor.BLUE, ChatColor.GRAY + "The " + GameRole.HOST.getPrefix() + " has been reset");
            }
            else {
                Player target = Bukkit.getPlayer(args[0]);
                if (target == null) {
                    if (commandSender instanceof Player) {
                        Player player = (Player) commandSender;
                        User user = uhc.getUserMap().get(player.getUniqueId());
                        user.sendBossBar(uhc, BarColor.RED, ChatColor.RED + "Player not found");
                        user.play(Sound.ENTITY_BLAZE_HURT);
                    } else
                        commandSender.sendMessage(ChatColor.RED + "Player not found");
                } else {
                    User targetuser = uhc.getUserMap().get(target.getUniqueId());
                    if (!targetuser.canChangeRole(GameRole.HOST)) {
                        if (commandSender instanceof Player) {
                            Player player = (Player) commandSender;
                            User user = uhc.getUserMap().get(player.getUniqueId());
                            user.sendBossBar(uhc, BarColor.RED, ChatColor.RED + "Player does not have permission to become a host");
                            user.play(Sound.ENTITY_BLAZE_HURT);
                        } else
                            commandSender.sendMessage(ChatColor.RED + "Player does not have permission to become a host");
                    } else {
                        uhc.setHost(targetuser);
                        targetuser.setRole(GameRole.HOST);
                        targetuser.play(Sound.ENTITY_PLAYER_LEVELUP);
                        target.sendMessage(UHC.PREFIX + "You are now " + GameRole.HOST.getPrefix());
                        uhc.broadcast(BarColor.BLUE, targetuser.getName() + ChatColor.GRAY + " is now " + GameRole.HOST.getPrefix());
                    }
                }
            }
        }
        return true;
    }
}
