package com.polaruhc.uhcplugin.commands;

import com.polaruhc.uhcplugin.UHC;
import com.polaruhc.uhcplugin.user.User;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.boss.BarColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collections;

public class ScatterCommand extends BasicCommand{
    private final UHC uhc;

    public ScatterCommand(UHC uhc) {
        super(uhc, "scattersolo", "Sccatters all the players for an FFA", "/<command> <world> <radius> <mindistance>", Collections.emptyList());
        this.uhc = uhc;
    }

    public boolean execute(CommandSender commandSender, String label, String[] args) {
        if(args.length < 3){
            if(commandSender instanceof Player){
                Player player = (Player) commandSender;
                User user = uhc.getUserMap().get(player.getUniqueId());
                user.sendBossBar(uhc, BarColor.RED, ChatColor.RED + "Invalid usage: " + ChatColor.YELLOW +  getUsage().replace("<command>", label));
                user.play(Sound.ENTITY_BLAZE_HURT);
            }
            else commandSender.sendMessage(ChatColor.RED + "Invalid usage: " + ChatColor.YELLOW + "/" + getUsage().replace("<command>", label));
        }
        else {
            World world = Bukkit.getWorld(args[0]);
            if (world == null) {
                if (commandSender instanceof Player) {
                    Player player = (Player) commandSender;
                    User user = uhc.getUserMap().get(player.getUniqueId());
                    user.sendBossBar(uhc, BarColor.RED, ChatColor.RED + "Player not found");
                    user.play(Sound.ENTITY_BLAZE_HURT);
                }
                else {
                    commandSender.sendMessage(ChatColor.RED + "Player not found");
                }
            }
            else {
                int radius;
                try{
                    radius = Integer.parseInt(args[1]);
                }
                catch (Exception ex){
                    if (commandSender instanceof Player) {
                        Player player = (Player) commandSender;
                        User user = uhc.getUserMap().get(player.getUniqueId());
                        user.sendBossBar(uhc, BarColor.RED, ChatColor.RED + "Invalid radius number " + ChatColor.YELLOW + args[1]);
                        user.play(Sound.ENTITY_BLAZE_HURT);
                    }
                    else commandSender.sendMessage(ChatColor.RED + "Invalid radius number " + ChatColor.YELLOW + args[1]);
                    return true;
                }
                int mindistance;
                try{
                    mindistance = Integer.parseInt(args[2]);
                }
                catch (Exception ex){
                    if (commandSender instanceof Player) {
                        Player player = (Player) commandSender;
                        User user = uhc.getUserMap().get(player.getUniqueId());
                        user.sendBossBar(uhc, BarColor.RED, ChatColor.RED + "Invalid mindistance number " + ChatColor.YELLOW + args[2]);
                        user.play(Sound.ENTITY_BLAZE_HURT);
                    }
                    else commandSender.sendMessage(ChatColor.RED + "Invalid mindistance number " + ChatColor.YELLOW + args[2]);
                    return true;
                }
                uhc.getScatterer().findPlaces(world, radius, mindistance, new Runnable() {
                    public void run() {
                        uhc.getScatterer().scatterSolos();
                    }
                });
            }
        }
        return true;
    }
}
