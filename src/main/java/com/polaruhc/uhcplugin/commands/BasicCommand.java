package com.polaruhc.uhcplugin.commands;

import com.polaruhc.uhcplugin.UHC;
import com.polaruhc.uhcplugin.util.Reflection;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.plugin.SimplePluginManager;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

public abstract class BasicCommand extends Command{
    private final UHC uhc;
    public BasicCommand(UHC uhc, String name, String description, String usageMessage, List<String> aliases){
            super(name, description, usageMessage, aliases);
        this.uhc = uhc;
    }

    public void register() throws NoSuchFieldException, IllegalAccessException{
        SimplePluginManager simplePluginManager = (SimplePluginManager)uhc.getServer().getPluginManager();
        SimpleCommandMap commandMap = Reflection.access(SimplePluginManager.class, simplePluginManager, "commandMap");
        register(commandMap);
        commandMap.register("uhc", this);
    }

    public static void unregisterAll() throws NoSuchFieldException, IllegalAccessException{
        SimplePluginManager simplePluginManager = (SimplePluginManager) Bukkit.getServer().getPluginManager();
        SimpleCommandMap commandMap = Reflection.access(SimplePluginManager.class, simplePluginManager, "commandMap");
        Map<String, Command> knownCommands = Reflection.access(SimpleCommandMap.class, commandMap, "knownCommands");
        for(Map.Entry<String, Command> commandEntry: new HashSet<>(knownCommands.entrySet())){
            if(commandEntry.getValue() instanceof BasicCommand){
                while(knownCommands.values().contains(commandEntry.getValue())){
                    knownCommands.values().remove(commandEntry.getValue());
                }
            }
        }
    }

    public void unregister() throws NoSuchFieldException, IllegalAccessException{
        SimplePluginManager simplePluginManager = (SimplePluginManager)uhc.getServer().getPluginManager();
        SimpleCommandMap commandMap = Reflection.access(SimplePluginManager.class, simplePluginManager, "commandMap");
        Map<String, Command> knownCommands = Reflection.access(SimpleCommandMap.class, commandMap, "knownCommands");
        while(knownCommands.values().contains(this)){
            knownCommands.values().remove(this);
        }
        unregister(commandMap);
    }
}
