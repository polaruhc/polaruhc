package com.polaruhc.uhcplugin.commands;

import com.polaruhc.uhcplugin.UHC;
import com.polaruhc.uhcplugin.game.GameState;
import com.polaruhc.uhcplugin.user.User;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.boss.BarColor;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Arrays;

public class FinishGameCommand extends BasicCommand{
    private final UHC uhc;

    public FinishGameCommand(UHC uhc) {
        super(uhc, "finishgame", "Ends the UHC game", "/<command>", Arrays.asList("endgame","finish","close"));
        this.uhc = uhc;
    }

    public boolean execute(CommandSender commandSender, String label, String[] args) {
        Bukkit.broadcastMessage(UHC.PREFIX + "Server is now restarting");
        uhc.setGameState(GameState.FINISHED);
        for(User user: uhc.getUsers()){
            if(user.isOnline() && !user.isStaff()){
                user.getPlayer().kickPlayer(uhc.getLoginListener().getLogin_header() + "The server is now closed" + uhc.getLoginListener().getLogin_footer());
            }
        }
        new BukkitRunnable(){
            int num = 5;
            public void run() {
                if(num == 0){
                    Bukkit.shutdown();
                    cancel();
                    return;
                }
                uhc.broadcast(Sound.BLOCK_NOTE_BLOCK_BASS);
                uhc.broadcast(BarColor.RED, ChatColor.DARK_RED + "Restarting in " + ChatColor.YELLOW + num);
                num--;
            }
        }.runTaskTimer(uhc, 20, 20);
        return false;
    }
}
