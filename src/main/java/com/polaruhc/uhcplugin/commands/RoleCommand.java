package com.polaruhc.uhcplugin.commands;

import com.polaruhc.uhcplugin.UHC;
import com.polaruhc.uhcplugin.game.GameRole;
import com.polaruhc.uhcplugin.user.User;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;

public class RoleCommand extends BasicCommand implements InventoryHolder, Listener{
    private final UHC uhc;

    public RoleCommand(UHC uhc) {
        super(uhc, "role", "Changes the role of a staff member", "/role [other] (role)", Arrays.asList("setrole"));
        this.uhc = uhc;
    }

    @Override
    public void register() throws NoSuchFieldException, IllegalAccessException {
        super.register();
        uhc.getServer().getPluginManager().registerEvents(this, uhc);
    }

    public boolean execute(CommandSender commandSender, String label, String[] args) {
        if(commandSender instanceof Player && args.length == 0){
            Player p = (Player)commandSender;
            User user = uhc.getUserMap().get(p.getUniqueId());
            Inventory inventory = Bukkit.createInventory(this, 9, ChatColor.RED + "Change role");
            update(user, inventory);
            user.play(Sound.ENTITY_PLAYER_LEVELUP);
            p.openInventory(inventory);
        }
        else if(args.length < 2){

        }
        else{

        }
        return true;
    }

    public void update(User user, Inventory inventory){
        int i = 2;
        for(GameRole gameRole: GameRole.values()){
            if(gameRole != GameRole.HOST) {
                ItemStack itemStack = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
                ItemMeta meta = itemStack.getItemMeta();
                String display;
                List<String> lore = Arrays.asList(ChatColor.GRAY + gameRole.getDescription());
                meta.setLore(lore);
                if (user.getRole() == gameRole) {
                    itemStack.setDurability(DyeColor.BLUE.getDyeData());
                    display = ChatColor.BLUE + "You are currently a " + gameRole.getPrefix();
                    ;
                } else if (user.canChangeRole(gameRole)) {
                    itemStack.setDurability(DyeColor.GREEN.getDyeData());
                    display = ChatColor.GREEN + "You have permission for a " + gameRole.getPrefix();
                } else {
                    itemStack.setDurability(DyeColor.RED.getDyeData());
                    display = ChatColor.RED + "You cannot become a " + gameRole.getPrefix();
                }
                meta.setDisplayName(display);
                itemStack.setItemMeta(meta);
                inventory.setItem(i, itemStack);
                i+= 2;
            }
        }
    }

    @EventHandler
    public void onPlayerClick(InventoryClickEvent e){
        if(e.getView().getTopInventory() != null && e.getWhoClicked() instanceof Player && e.getView().getTopInventory().getHolder() == this){
            Player player = (Player) e.getWhoClicked();
            User user = uhc.getUserMap().get(player.getUniqueId());
            int slot = e.getRawSlot();
            int gameroleid = (slot - 2) / 2;
            if(gameroleid >= 0 && gameroleid < GameRole.values().length-1){
                GameRole gameRole = GameRole.getRole(gameroleid);
                if(user.getRole() == gameRole){
                    user.play(Sound.ENTITY_BLAZE_HURT);
                    user.sendBossBar(uhc, BarColor.RED, ChatColor.RED + "You are already a " + gameRole.getPrefix());
                }
                else if(user.canChangeRole(gameRole)){
                    user.play(Sound.ENTITY_PLAYER_LEVELUP);
                    user.setRole(gameRole);
                    user.sendBossBar(uhc, BarColor.BLUE, ChatColor.GRAY + "You are now a " + gameRole.getPrefix());
                    update(user, e.getView().getTopInventory());
                }
                else{
                    user.play(Sound.ENTITY_BLAZE_HURT);
                    user.sendBossBar(uhc, BarColor.RED, ChatColor.RED + "You do not have permission for this role");
                }

            }
            e.setCancelled(true);
        }
    }

    public Inventory getInventory(){
        return null;
    }
}